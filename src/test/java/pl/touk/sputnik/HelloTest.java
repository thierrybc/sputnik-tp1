package pl.touk.sputnik;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class HelloTest {
    private final Hello hello = new Hello();

    @Test
    void shouldReturnSameMessage() {
        assertThat(hello.greet()).isEqualTo("Hello from Sputnik");
    }
}
